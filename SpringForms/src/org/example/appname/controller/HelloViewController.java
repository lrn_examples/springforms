package org.example.appname.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

// https://www.tutorialspoint.com/spring/spring_mvc_hello_world_example.htm
// https://habr.com/ru/post/336816/

// access app with:
// http://localhost:8080/HelloWeb/view1
// HelloWeb is (web) application context (root)

@Controller // Spring MVC Controller (Controller Bean)
// 1). view1 DON'T EXIST PHYSICALLY!  It's PRESENT IN URL ONLY!
// but this name SHALL NOT MATCH ANYTHING - IT CAN BE ANYTHING! 
@RequestMapping("/view1") // for all methods
//@RequestMapping("/helloView") // IS BETTER! (NAMING CONVENTION) !!!
public class HelloViewController {
	
	// only for this method
	@RequestMapping(method=RequestMethod.GET)
	public String showHelloView(ModelMap model) {		
		model.addAttribute("mymsg", "My1st message");
		return "helloView"; // view name = WEB-INF/views/helloView.jsp		
	}

}
