package org.example.appname.controller;

import org.example.appname.domain.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/form1")
public class SpringForm1Controller {
	
	
	@RequestMapping(method=RequestMethod.GET)
	public String showForm1(@ModelAttribute("anyName") Person model) {	
		model.setAge(23);
		model.setName("John");
		
		return "springForm1";		
	}
	
	
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmitForm1( @ModelAttribute("myModelAttrName") Person model) {
		
//		System.out.println("onSubmitForm1(): NAME: " + model.get);
//		System.out.println("onSubmitForm1(): AGE: " + model.getAge());
		
		return "springForm1SubmitResult";
	}

	
//@RequestMapping(method=RequestMethod.GET)
//public ModelAndView showForm1() {
//	ModelAndView mav = new ModelAndView("springForm1");
//	System.out.println("IN CONTROLLER XXXXXXXXXXXXXXXXXXXXXXXXXXX");
//	mav.addObject("age", 23);
//	mav.addObject("name", "mary");
//	mav.addObject("1121");
//	
//	return mav;		
//}
	
	
}
