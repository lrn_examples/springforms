package org.example.appname.domain;

import java.io.Serializable;

// w/o Serializable - works OK, no ERRORs.
// Yes, JavaBean shall be Serializable!
// But it's OK without Serializable
// (obj->XML/JSON String is also serialization = stream of bytes)
public class Person implements Serializable {
	public Person() {
	}

	private String name;
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	
//	public Person(String name, String age) {
//		super();
//		this.name = name;
//		this.age = age;
//	}

}
