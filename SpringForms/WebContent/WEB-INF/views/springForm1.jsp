<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Form 1 - Example of Spring Form</title>
</head>
<body>
        <p>Name = ${anyName.name}
    <p>
        <p> Age = ${anyName.age}
        
   
        <form:form action="/SpringForms/form1" method="POST"
            modelAttribute="anyName">

            <form:input path="age" />
            <p>
                <form:input path="name" />
            <p>
                <button type="submit">Submit</button>
        </form:form>

</body>
</html>